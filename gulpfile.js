var gulp = require('gulp');
var sass = require('gulp-sass');

var connect = require('gulp-connect');
var flatten = require('gulp-flatten');
var cleanCSS = require('gulp-clean-css');

var paths = {
  livereload: ['./dist/**/*'],
  sass: [
    './src/sass/**/*.scss',
    '!./src/sass/mixins/**',
    '!./src/sass/**/*[-]*.scss'
  ]
};

gulp.task('sass', function() {
  return gulp.src(paths.sass)
  .pipe(sass().on('error', sass.logError))
  .pipe(cleanCSS({keepSpecialComments: 0}))
  .pipe(flatten())
  .pipe(gulp.dest('./dist/css'));
});

gulp.task('server', function() {
  connect.server({
    livereload: true
  });
});

gulp.task('livereload', function() {
  return gulp.src(paths.livereload)
  .pipe(connect.reload());
});

gulp.task('watch', function() {
  gulp.watch('./src/sass/**/*', ['sass']);
  gulp.watch(paths.livereload, ['livereload']);
});

gulp.task('default', ['sass', 'server', 'watch']);